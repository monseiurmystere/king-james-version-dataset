#!/bin/bash
rm kjv.osis.xml kjva.osis.xml kjva.xml
##Création de kjv.osis.xml
sed 's#<note type="x-*.*</note>##g' kjvfull.xml >kjv.osis.xml
sed -i 's#<milestone type="x-extra-p"/>##g' kjv.osis.xml
sed -i 's#<milestone type="x-strongsMarkup" resp="[a-z0-9: -]*"/>##g' kjv.osis.xml

#Modif de Kjvdc
sed 's#\t</osisText>##g;s#</osis>##g' kjvdc.xml >a
sed -i '/^$/d' a

##Supprime le début du fichier a qui donne b
sed '1,45d' a >b

##Fusionne b avec kjv.osis.xml pour donner kjva.osis.xml


NT_LINE="<div type=\"bookGroup\" canonical=\"true\" subType=\"x-NT\">"
while IFS= read -r LINE ; do
if [ "$LINE" == "$NT_LINE" ] ; then cat b >> kjva.xml
fi
echo "$LINE" >> kjva.xml
done < kjv.osis.xml


#Copie les lignes 4 à 35 dans le fichier patch
sed -n 5,35p kjvdc.xml >patch

#pcregrep -Mn '</revisionDesc>\n  <work osisWork="KJV">' kjvfull.xml >out
#sed -ri 's/([0-9]*):<\/revisionDesc>/\1/g;s/  <work osisWork="KJV">//g;/^$/d' out
##Colle le ficher patch après la ligne 63 dans kjva.osis.xml ajouter le nombre de nouvelle ligne si besoin
#sed -i 63rpatch kjva.osis.xml

NT_LINE='<work osisWork="KJV">'
while IFS= read -r LINE ; do
if [[ "$LINE" == *"$NT_LINE" ]] ; then cat patch >> kjva.osis.xml
fi
echo "$LINE" >> kjva.osis.xml
done < kjva.xml

sed -ri 's/King James Version \(1769\) with Strongs Numbers and Morphology/King James Version \(1769\) with Apocrypha/g;s/osisWork="KJV"/osisWork="KJVA"/g;s/OSIS">Bible\.KJV</OSIS">Bible\.KJVA</g;s/osisIDWork="KJV"/osisIDWork="KJVA"/g' kjva.osis.xml

xmllint --noout --schema ~/.bin/schema/osisCore.2.1.1-cw-latest.xsd kjva.osis.xml kjv.osis.xml kjvfull.xml
#osis2mod ~/.sword/modules/texts/ztext/kjvdc/ kjva.osis.xml -z -v KJVA
rm a b patch
